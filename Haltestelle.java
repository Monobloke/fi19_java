import java.util.ArrayList;

public class Haltestelle {

    String name;    

    ArrayList<ArrayList<Haltestelle>> anschlusshaltestellen;

    public Haltestelle (String name,
                        ArrayList<ArrayList<Haltestelle>> anschlusshaltestellen) {

        setName(name);
        setAnschlusshaltestellen(anschlusshaltestellen);
    }

    // Konstruktor nur fuer Namen
    public Haltestelle (String name) {

        this(name, null);
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAnschlusshaltestellen
    (ArrayList<ArrayList<Haltestelle>> anschlusshaltestellen) {
        this.anschlusshaltestellen = anschlusshaltestellen;
    }
}