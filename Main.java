import java.util.ArrayList;

public class Main {

    public static void main(String[]args) {

        // Hier ein Beispiel fuer die Haltestelle Belvedere
        // nur mit den Anschluessen der Linie 11
    
        // zunaechst Hasltestellenobjekte erzeugen, die noch keine
        // Listen mit Anschlusshaltestellen haben
        Haltestelle hanssenstr = new Haltestelle("Hanssenstr.");
        Haltestelle hardenbergstr = new Haltestelle("Hardenbergstr.");

        // neue Liste von Anschlusshaltestellen der Linie 11 an der
        // Haltestelle "Belvedere"
        ArrayList<Haltestelle> belvedereElfAnschluss =
            new ArrayList<Haltestelle>();

        // von Belvedere gelangt man mit der Linie 11 in die
        // Hanssenstrasse und in die Hardenbergstrasse
        belvedereElfAnschluss.add(hanssenstr);
        belvedereElfAnschluss.add(hardenbergstr);

        // dasselbe fuer die anderen Linien, die die Hst. Belvedere
        // bedienen

        // Liste von Listen von Anschlusshaltestellen der Haltestelle
        // Belvedere
        ArrayList<ArrayList<Haltestelle>> belvedereAnschluesse
            = new ArrayList<ArrayList<Haltestelle>>();

        // zu den Anschlusshaltestellen der Haltestelle Belvedere
        // gehoeren diejenigen, die man mit der Linie 11 erreicht
        belvedereAnschluesse.add(belvedereElfAnschluss);

        // hier noch weitere "add"-Aufrufe fuer die anderen Linien,
        // die die Hst. Belvedere bedienen

        // jetzt sind alle Daten fuer die Haltestelle Belvedere
        // bekannt, und wir koennen sie erzeugen
        Haltestelle belvedere = new Haltestelle("Belvedere", belvedereAnschluesse);



        // Hier dasselbe fuer die Haltestelle "Feldstrasse/Waitzstrasse"

        

        // Hier dasselbe fuer die Haltestelle "Uni-Kliniken"



        // Hier dasselbe fuer die Haltestelle "Wrangelstrasse"



        // Hier dasselbe fuer die Haltestelle "Niemannsweg"



        // Hier dasselbe fuer die Haltestelle "Landtag"



        // Hier dasselbe fuer die Haltestelle "Reventloubruecke"



        // Hier dasselbe fuer die Haltestelle "Schwanenweg"



        

        System.out.println("kvg");

    }
}